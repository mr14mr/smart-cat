var app = require('express')();
var http = require('http').Server(app);
var express = require('express');
var path = require('path');
var math = require('math');
var request = require('request');

var io = require('socket.io')(http);

function setEmotion(screen, emotion) {

  var firstLine;
  var secondLine;

  switch (emotion) {
    case "happy":
      screen.setColor(100, 100, 255);
      //firstLine = "\\";
      firstLine = "    ^       ^  ";
      secondLine ="        V      ";
      break;
    case "angry": 
      screen.setColor(255, 0, 0);
      firstLine = "   -T-   -T-     ";
      secondLine ="    |     |     ";
      break;
  }   

  screen.setCursor(0, 0);
  screen.write(firstLine);
  screen.setCursor(1, 0);
  screen.write(secondLine);
}


app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(req, res){
  res.sendfile(__dirname + '/index.html');
});

http.listen(8080, "0.0.0.0", function(){
  console.log('listening on *:8080');
});

var Cylon = require('cylon');
var fs = require('fs');
var querystring = require('querystring');

var socket = require('socket.io-client')('http://smart-cat1.cloudapp.net:3000');

socket.on('connect', function(){
	console.log('connect');
});
socket.on('event', function(data){
	console.log('data:' + data);

});
socket.on('disconnect', function(){
	console.log('disconnect');
});
socket.on('say', function(data){
	if(data.indexOf("погода") > -1) {
		data = "Солнечная, зонтик не забудь!";
	}
	else if(data.indexOf("машина") > -1) {
		data = "Не люблю говорить об этом!";
	} else if(data.indexOf("люблю тебя") > -1) {
		data = "И я тебя люблю... Но интел больше!";
	}
	else  {
		data = "Давай поговорим о другом.";
	}
	var fs = require('fs');
	fs.writeFile("public/msg.json", JSON.stringify(data) , function(err) {
		if(err) {
		    console.log(err);
		} else {
		    console.log("Файл сохранен.");
		}
    request({
      url: "http://10.1.79.126:3000/?text=" + encodeURIComponent(data),
      method: "GET",
      headers: {
        "Content-Type": "application/json; charset=utf-8"
      }}, function(error, response, body){
        if(error) {
            console.log(error);
        } else {
            console.log(response.statusCode, body);
        }
    });
	});

});
//var msg = "";

var B = 3975;

var code = 0;


Cylon.robot({
  connections: {
    edison: { adaptor: 'intel-iot'},
  },

  devices: {
    led: { driver: 'led', pin: 13, connection: 'edison' },
    temperature: { driver: 'analog-sensor', pin: 0, lowerLimit: 100, upperLimit: 900 },
    air: { driver: 'analog-sensor', pin: 1, lowerLimit: 100, upperLimit: 900 },
    vibration: {driver: 'analog-sensor', pin: 2, lowerLimit: 100, upperLimit: 900 },
    screen: { driver: "upm-jhd1313m1", connection: 'edison' },
  },

  writeMessage: function (message, color) {
    var that = this;
    var str = message.toString();
    while (str.length < 16) {
      str = str + " ";
    }
    console.log(message);
    that.screen.setCursor(0,0);
    that.screen.write(str);
    switch(color)
    {
      case "red":
        that.screen.setColor(255, 0, 0);
        break;
      case "green":
        that.screen.setColor(0, 255, 0);
        break;
      case "blue":
        that.screen.setColor(0, 0, 255);
        break;
      default:
        that.screen.setColor(255, 255, 255);
        break;
    }
  },

  

  work: function(my) {

	setInterval(function(){
		my.led.toggle();
	}, 500);

  every((1).second(), function() {
      var vibValue = my.vibration.analogRead();
      //console.log(vibValue);

      if  (vibValue > 520 || vibValue < 480) {
        setEmotion(my.screen, "angry");
      } else {
        setEmotion(my.screen, "happy");
      }
  })

    every((2).second(), function() {
    	tempValue = my.temperature.analogRead();
	    tempValue = (1023-tempValue)*10000/tempValue;
        tempValue = Math.round(1/(math.log(tempValue/10000)/B+1/298.15)-273.15);

    	fs.writeFile(__dirname + "/public/temperature.json", tempValue-10, function(err) {
		    if(err) {
		        return console.log(err);
		    }
		}); 
		console.log('t=', tempValue);

		airValue = my.air.analogRead();
    	fs.writeFile(__dirname + "/public/air.json", airValue, function(err) {
		    if(err) {
		        return console.log(err);
		    }
		}); 
		console.log('air=', airValue);
	});

    every((60).second(), function() {
	    request({
		 	url:     'http://smart-cat1.cloudapp.net/api/data',
//		    qs: {from: 'blog example', time: +new Date()}, //Query string data
		    method: 'POST', //Specify the method
		    headers: { //We can define headers too
		        'Content-Type': 'application/x-www-form-urlencoded',
		        'Custom-Header': 'Custom Value'
		    },
		    form: {
		    	temperature: tempValue,
		    	airquality: airValue
		 	}
		}, function(error, response, body){
		    if(error) {
		        console.log(error);
		    } else {
		        console.log(response.statusCode, body);
		    }
		});

    });

  }
}).start();